Alternative to https://github.com/spiegeleixxl/html5-ass-subtitles using https://github.com/sykopomp/mona , instead of
a custom parser. It's intended to tokenize arbitrary subtitle files, currently supporting ASS and SRT.

Displaying these files is outside the scope of this project, which is intended for use in a version of OceanusStreamer.